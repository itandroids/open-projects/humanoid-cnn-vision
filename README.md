# humanoid-cnn-vision

Files for the humanoid cnn vision paper.
`CNN-Paper-Training` contains the training code for the neural networks discussed in the paper and `CNN-Paper-Testing` contains the testing code.